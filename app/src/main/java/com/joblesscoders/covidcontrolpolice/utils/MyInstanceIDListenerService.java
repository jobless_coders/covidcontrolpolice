package com.joblesscoders.covidcontrolpolice.utils;

import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.joblesscoders.covidcontrolpolice.admin.AdminDashboardActivity;
import com.joblesscoders.covidcontrolpolice.pojo.Reply;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyInstanceIDListenerService extends FirebaseMessagingService {
    @Override
    public void onNewToken(String s) {
        RestApiHandler.getAPIService().storeToken(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3),s).enqueue(new Callback<Reply>() {
            @Override
            public void onResponse(Call<Reply> call, Response<Reply> response) {
                if(response.code() == 200&&response.body().getMessage().equals("success"))
                {
                    try {
                        Toast.makeText(getApplicationContext(), "Token updated", Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e)
                    {

                    }

                }
            }

            @Override
            public void onFailure(Call<Reply> call, Throwable t) {

            }
        });
        super.onNewToken(s);
    }
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

    }

}
