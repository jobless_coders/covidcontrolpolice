package com.joblesscoders.covidcontrolpolice.utils;

import com.joblesscoders.covidcontrolpolice.pojo.PoliceAuth;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnelReply;
import com.joblesscoders.covidcontrolpolice.pojo.PoliceStation;
import com.joblesscoders.covidcontrolpolice.pojo.Reply;
import com.joblesscoders.covidcontrolpolice.pojo.Report;
import com.joblesscoders.covidcontrolpolice.pojo.ReportReply;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RestApiInterface {

   @GET("loginPoliceAdmin/{number}")
   Call<PoliceAuth> loginPoliceAdmin(@Path("number") String number);

   @POST("createPoliceStation")
   Call<Reply> createPoliceStation(@Body PoliceStation body);

   @GET("getAllPoliceStation")
   Call<Reply> getAllPoliceStation();

   @GET("loginPolicePersonnel/{number}")
   Call<PoliceAuth> loginPolicePersonnel(@Path("number") String number);

   @POST("createPolicePersonnel")
   Call<Reply> createPolicePersonnel(@Body PolicePersonnel body);

    @GET("getPolicePersonnel")
    Call<PolicePersonnelReply> getPolicePersonnelByPhone(@Query("phone") String phone);

    @GET("getPolicePersonnel")
    Call<PolicePersonnelReply> getPolicePersonnelById(@Query("police_id") String id);

    @GET("getAllPolicePersonnel")
    Call<PolicePersonnelReply> getAllPolicePersonnel(@Query("station_id") String id);
    ///getAllPolicePersonnel/?station_id =&isApproved=
    @GET("getAllPolicePersonnel")
    Call<PolicePersonnelReply> getAllPolicePersonnel(@Query("station_id") String id,@Query("isApproved") boolean isApproved);

    @GET("getReports/{station_id}")
    Call<ReportReply> getReports(@Path("station_id") String id, @Query("isAttended")boolean isApproved);

    @GET("updatePolicePersonnel")
    Call<Reply> updatePolicePersonnel(@Query("isApproved") boolean isApproved,@Query("personnel_id") String personnel_id);

    @POST("updateReport")
    Call<Reply> updateReport(@Body Report body);
    ///storeToken/:police_phone?token=’PUSH_TOKEN’

    @GET("storeToken/{police_phone}")
    Call<Reply> storeToken(@Path("police_phone") String phone,@Query("token") String token);






}
