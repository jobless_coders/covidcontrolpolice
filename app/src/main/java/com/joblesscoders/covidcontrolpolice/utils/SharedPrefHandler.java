package com.joblesscoders.covidcontrolpolice.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;

public class SharedPrefHandler {


    public static void saveData(PolicePersonnel policePersonnelReply, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("type", policePersonnelReply.getType());
        editor.putString("name", policePersonnelReply.getName());
        editor.putString("id", policePersonnelReply.get_id());
        editor.putString("phone", policePersonnelReply.getPhone());
        editor.putInt("pin", policePersonnelReply.getPin());
        editor.putString("policestationname", policePersonnelReply.getPoliceStationName());
        editor.putString("policestationid", policePersonnelReply.getPoliceStationId());
        editor.commit();

    }
    public static String isType(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        String type = sharedPreferences.getString("type",null);
        return type;
    }
    public static PolicePersonnel getPoliceData(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo",Context.MODE_PRIVATE);
        PolicePersonnel policePersonnel = new PolicePersonnel(sharedPreferences.getString("name",null),sharedPreferences.getString("policestationname",null),sharedPreferences.getString("phone",null),sharedPreferences.getString("policestationid",null),sharedPreferences.getString("type",null),sharedPreferences.getString("id",null),sharedPreferences.getInt("pin",0));
        return policePersonnel;
    }
}
