package com.joblesscoders.covidcontrolpolice.pojo;

import java.util.List;

public class PolicePersonnelReply {
    private String status,message;
    private List<PolicePersonnel> data;

    public PolicePersonnelReply(String status, String message, List<PolicePersonnel> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<PolicePersonnel> getData() {
        return data;
    }
}
