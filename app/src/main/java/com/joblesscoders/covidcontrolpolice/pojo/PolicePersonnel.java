package com.joblesscoders.covidcontrolpolice.pojo;

public class PolicePersonnel {
    private String name,policeStationName,phone,policeStationId,type;
    private boolean isApproved;
    private int pin;
    private String _id;

    public PolicePersonnel(String name, String policeStationName, String phone, String policeStationId, boolean isApproved, int pin) {
        this.name = name;
        this.policeStationName = policeStationName;
        this.phone = phone;
        this.policeStationId = policeStationId;
        this.isApproved = isApproved;
        this.pin = pin;
    }

    public PolicePersonnel(String name, String policeStationName, String phone, String policeStationId, String type, String _id,int pin) {
        this.name = name;
        this.policeStationName = policeStationName;
        this.phone = phone;
        this.policeStationId = policeStationId;
        this.type = type;
        this.pin = pin;
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public String getPoliceStationName() {
        return policeStationName;
    }

    public String getPhone() {
        return phone;
    }

    public String getPoliceStationId() {
        return policeStationId;
    }

    public String getType() {
        return type;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public int getPin() {
        return pin;
    }

    public String get_id() {
        return _id;
    }
}
