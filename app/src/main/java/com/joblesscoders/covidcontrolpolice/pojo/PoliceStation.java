package com.joblesscoders.covidcontrolpolice.pojo;

public class PoliceStation {
  /*  headName:
    policeStationName:
    phone:(string)
    address:
    latitude:
    longitude:
    pin(for nichu police/mukherjee):from backend*/
    private String headName,policeStationName,phone,address;
    private double latitude,longitude;
    private int pin;
    private String _id;

    public PoliceStation(String headName, String policeStationName, String phone, String address, double latitude, double longitude, int pin, String id) {
        this.headName = headName;
        this.policeStationName = policeStationName;
        this.phone = phone;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.pin = pin;
        this._id = id;
    }

    public PoliceStation(String headName, String policeStationName, String phone, String address, double latitude, double longitude) {
        this.headName = headName;
        this.policeStationName = policeStationName;
        this.phone = phone;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getHeadName() {
        return headName;
    }

    public String getPoliceStationName() {
        return policeStationName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getPin() {
        return pin;
    }

    public String getId() {
        return _id;
    }
}
