package com.joblesscoders.covidcontrolpolice.pojo;

import java.util.List;

public class PolicePersonnelListReply {
    private String status,message;
    private List<PolicePersonnel> data;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<PolicePersonnel> getData() {
        return data;
    }
}
