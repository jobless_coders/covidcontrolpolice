package com.joblesscoders.covidcontrolpolice.pojo;


import android.os.Parcel;
import android.os.Parcelable;

public class Report implements Parcelable {

    private String reportDetails,reportType,imageUrl,policeStationId,phone,username,policeStationName,personnel_id,attendedByName,report_id,_id,attendedBy;
    private int score;
    private long timestamp,attendedTime;
    private boolean isAttended;
    private double latitude,longitude;

 public Report(String attendedByName, String report_id, String attendedBy, long attendedTime, boolean isAttended) {
  this.attendedByName = attendedByName;
  this.report_id = report_id;
  this.attendedBy = attendedBy;
  this.attendedTime = attendedTime;
  this.isAttended = isAttended;
 }

 public void setAttendedByName(String attendedByName) {
  this.attendedByName = attendedByName;
 }

 public void setReport_id(String report_id) {
  this.report_id = report_id;
 }

 public void setAttendedBy(String attendedBy) {
  this.attendedBy = attendedBy;
 }

 public void setAttendedTime(long attendedTime) {
  this.attendedTime = attendedTime;
 }

 public void setAttended(boolean attended) {
  isAttended = attended;
 }

 protected Report(Parcel in) {
  reportDetails = in.readString();
  reportType = in.readString();
  imageUrl = in.readString();
  policeStationId = in.readString();
  phone = in.readString();
  username = in.readString();
  policeStationName = in.readString();
  personnel_id = in.readString();
  attendedByName = in.readString();
  report_id = in.readString();
  _id = in.readString();
  score = in.readInt();
  timestamp = in.readLong();
  attendedTime = in.readLong();
  isAttended = in.readByte() != 0;
  latitude = in.readDouble();
  longitude = in.readDouble();
 }

 public static final Creator<Report> CREATOR = new Creator<Report>() {
  @Override
  public Report createFromParcel(Parcel in) {
   return new Report(in);
  }

  @Override
  public Report[] newArray(int size) {
   return new Report[size];
  }
 };

 public String getReportDetails() {
  return reportDetails;
 }

 public String getReportType() {
  return reportType;
 }

 public String getImageUrl() {
  return imageUrl;
 }

 public String getPoliceStationId() {
  return policeStationId;
 }

 public String getPhone() {
  return phone;
 }

 public String getUsername() {
  return username;
 }

 public String getPoliceStationName() {
  return policeStationName;
 }

 public String getPersonnel_id() {
  return personnel_id;
 }

 public String getAttendedByName() {
  return attendedByName;
 }

 public String getReport_id() {
  return report_id;
 }

 public String get_id() {
  return _id;
 }

 public int getScore() {
  return score;
 }

 public long getTimestamp() {
  return timestamp;
 }

 public long getAttendedTime() {
  return attendedTime;
 }

 public boolean isAttended() {
  return isAttended;
 }

 public double getLatitude() {
  return latitude;
 }

 public double getLongitude() {
  return longitude;
 }

 @Override
 public int describeContents() {
  return 0;
 }

 @Override
 public void writeToParcel(Parcel parcel, int i) {
  parcel.writeString(reportDetails);
  parcel.writeString(reportType);
  parcel.writeString(imageUrl);
  parcel.writeString(policeStationId);
  parcel.writeString(phone);
  parcel.writeString(username);
  parcel.writeString(policeStationName);
  parcel.writeString(personnel_id);
  parcel.writeString(attendedByName);
  parcel.writeString(report_id);
  parcel.writeString(_id);
  parcel.writeInt(score);
  parcel.writeLong(timestamp);
  parcel.writeLong(attendedTime);
  parcel.writeByte((byte) (isAttended ? 1 : 0));
  parcel.writeDouble(latitude);
  parcel.writeDouble(longitude);
 }
}
