package com.joblesscoders.covidcontrolpolice.pojo;

public class PoliceAuth {
    boolean isNumberVerified;
    boolean isNumberRegistered;
    boolean isApproved, isAdmin;

    public PoliceAuth(boolean isNumberRegistered, boolean isApproved, boolean isAdmin) {
        this.isNumberRegistered = isNumberRegistered;
        this.isApproved = isApproved;
        this.isAdmin = isAdmin;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public PoliceAuth(boolean isNumberVerified, boolean isNumberRegistered) {
        this.isNumberVerified = isNumberVerified;
        this.isNumberRegistered = isNumberRegistered;
    }

    public boolean isNumberVerified() {
        return isNumberVerified;
    }

    public boolean isNumberRegistered() {
        return isNumberRegistered;
    }
}
