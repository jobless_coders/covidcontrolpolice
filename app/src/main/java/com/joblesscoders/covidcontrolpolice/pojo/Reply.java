package com.joblesscoders.covidcontrolpolice.pojo;

import java.util.List;

public class Reply {
    private String status,message;
    private List<PoliceStation> data;

    public Reply(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<PoliceStation> getData() {
        return data;
    }
}
