package com.joblesscoders.covidcontrolpolice.pojo;

import java.util.List;

public class ReportReply {
    private String status,message;
    private List<Report> data;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<Report> getData() {
        return data;
    }
}
