package com.joblesscoders.covidcontrolpolice.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.joblesscoders.covidcontrolpolice.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private View admin,non_admin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        admin = findViewById(R.id.policeadmin);
        non_admin = findViewById(R.id.policenonadmin);
        admin.setOnClickListener(this);
        non_admin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
    switch (view.getId())
    {
        case R.id.policenonadmin:
            Intent intent = new Intent(MainActivity.this, AuthActivity.class);
            intent.putExtra("type","nonadmin");
            startActivity(intent);


            break;
        case R.id.policeadmin:
             intent = new Intent(MainActivity.this,AuthActivity.class);
            intent.putExtra("type","admin");
            startActivity(intent);

            break;
    }
    }
}
