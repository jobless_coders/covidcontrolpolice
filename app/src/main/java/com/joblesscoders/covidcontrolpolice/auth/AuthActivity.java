package com.joblesscoders.covidcontrolpolice.auth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.joblesscoders.covidcontrolpolice.admin.AdminDashboardActivity;
import com.joblesscoders.covidcontrolpolice.admin.PoliceStationRegistrationActivity;
import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.nonadmin.NonAdminDashboardActivity;
import com.joblesscoders.covidcontrolpolice.nonadmin.NonAdminRegistrationActivity;
import com.joblesscoders.covidcontrolpolice.pojo.PoliceAuth;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnelReply;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;
import com.joblesscoders.covidcontrolpolice.utils.SharedPrefHandler;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = AuthActivity.class.getSimpleName();
    private FirebaseAuth mAuth;
    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String phoneNumber;
    private View send_otp,verify_otp,otpll,phonell,resend_otp;
    private EditText phone;
    private OtpEditText otpEditText;
    private PoliceAuth policeAuth;
    private String type;
    private TextView errormessage;
    private LinearLayout loaderLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        type = getIntent().getExtras().getString("type");
        mAuth = FirebaseAuth.getInstance();
        otpEditText = findViewById(R.id.et_otp);
        send_otp = findViewById(R.id.send);
        send_otp.setOnClickListener(this);
        verify_otp = findViewById(R.id.verifyotp);
        resend_otp = findViewById(R.id.resendotp);
        otpll = findViewById(R.id.otpll);

        phonell = findViewById(R.id.phonell);
        verify_otp.setOnClickListener(this);
        resend_otp.setOnClickListener(this);
        phone = findViewById(R.id.phone);
        loaderLayout = findViewById(R.id.loaderLayout);
        errormessage = findViewById(R.id.errortext);
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "onVerificationCompleted:" + credential);
                signInWithPhoneAuthCredential(credential);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                loaderLayout.setVisibility(View.GONE);
                Log.w(TAG, "onVerificationFailed", e);
                errormessage.setText(e.getMessage());
                Toast.makeText(AuthActivity.this, e.getMessage()+"", Toast.LENGTH_SHORT).show();



                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...

                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                phonell.setVisibility(View.GONE);
                otpll.setVisibility(View.VISIBLE);

                // ...
            }
        };
    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        //  FirebaseU mAuth;
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(AuthActivity.this, R.string.otp_verified, Toast.LENGTH_SHORT).show();
                            FirebaseUser user = task.getResult().getUser();
                            if(type.equals("admin"))
                            {
                                Log.d(TAG, "signInWithCredential:success");
                                signInAdmin();
                            }
                            else {
                                signInNonAdmin();
                            }
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");


                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(AuthActivity.this, "Verification code was invalid", Toast.LENGTH_SHORT).show();

                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    private void signInNonAdmin() {
        if(!policeAuth.isNumberRegistered())  {
            Log.d(TAG, "number not registered");
            startActivity(new Intent(AuthActivity.this, NonAdminRegistrationActivity.class));
        }
        else {
            Log.d(TAG, "number registered");
            if (policeAuth.isApproved()) {
                Log.d(TAG, "fetching data from api");
                getNonAdminData();
                //send to dashboard
            } else {
                Log.d(TAG, "not approved yet");
                errormessage.setText("You're not approved yet, ask your head to approve you.");
            }
        }
    }

    private void getNonAdminData() {
        RestApiHandler.getAPIService().getPolicePersonnelByPhone(phoneNumber.substring(3)).enqueue(new Callback<PolicePersonnelReply>() {
            @Override
            public void onResponse(Call<PolicePersonnelReply> call, Response<PolicePersonnelReply> response) {
                if(response.code() == 200)
                {
                    PolicePersonnelReply policePersonnelReply = response.body();
                    SharedPrefHandler.saveData(policePersonnelReply.getData().get(0),AuthActivity.this);
                    Intent intent = new Intent(AuthActivity.this, NonAdminDashboardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(AuthActivity.this, "Logged in successfully", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call<PolicePersonnelReply> call, Throwable t) {

            }
        });


    }

    private void signInAdmin() {

        if (policeAuth.isNumberVerified() && !policeAuth.isNumberRegistered()) {
            startActivity(new Intent(AuthActivity.this, PoliceStationRegistrationActivity.class));
        } else if (policeAuth.isNumberVerified() && policeAuth.isNumberRegistered()) {
            //start app

            getAdminData();

        }
    }

    private void getAdminData() {
        RestApiHandler.getAPIService().getPolicePersonnelByPhone(phoneNumber.substring(3)).enqueue(new Callback<PolicePersonnelReply>() {
            @Override
            public void onResponse(Call<PolicePersonnelReply> call, Response<PolicePersonnelReply> response) {
              //  Toast.makeText(AuthActivity.this, response.body()+"", Toast.LENGTH_SHORT).show();
                if(response.code() == 200)
                {
                    PolicePersonnelReply policePersonnelReply = response.body();
                    SharedPrefHandler.saveData(policePersonnelReply.getData().get(0),AuthActivity.this);
                    Intent intent = new Intent(AuthActivity.this, AdminDashboardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(AuthActivity.this, "Logged in successfully", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<PolicePersonnelReply> call, Throwable t) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
                loaderLayout.setVisibility(View.VISIBLE);
                phoneNumber = phone.getText().toString();
                if (phoneNumber.length() < 10) {
                    Toast.makeText(this, "Not a valid phone number!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(type.equals("admin"))
                {
                    verifyAdmin();
                }
                else
                {
                    verifyNonAdmin();
                }



                break;
            case R.id.resendotp:

                loaderLayout.setVisibility(View.VISIBLE);
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        phoneNumber,        // Phone number to verify
                        60,                 // Timeout duration
                        TimeUnit.SECONDS,   // Unit of timeout
                        this,               // Activity (for callback binding)
                        mCallbacks,mResendToken);        // OnVerificationStateChangedCallbacks

                break;

            case R.id.verifyotp:
                String code = otpEditText.getText().toString();
                if(code.length()<6)
                {
                    Toast.makeText(this, "OTP invalid", Toast.LENGTH_SHORT).show();
                    return;
                }
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
                signInWithPhoneAuthCredential(credential);
                break;
        }
    }

    private void verifyNonAdmin() {
        RestApiHandler.getAPIService().loginPolicePersonnel(phoneNumber).enqueue(new Callback<PoliceAuth>() {
            @Override
            public void onResponse(Call<PoliceAuth> call, Response<PoliceAuth> response) {
                if(response.code() == 200)
                {
                    policeAuth = response.body();
                    if(policeAuth.isAdmin())
                    {
                        Toast.makeText(AuthActivity.this, "You're an admin, try logging in as an admin", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        startLogin();
                    }
                }
            }

            @Override
            public void onFailure(Call<PoliceAuth> call, Throwable t) {

            }
        });

    }

    private void verifyAdmin() {
        RestApiHandler.getAPIService().loginPoliceAdmin(phoneNumber).enqueue(new Callback<PoliceAuth>() {
            @Override
            public void onResponse(Call<PoliceAuth> call, Response<PoliceAuth> response) {
                policeAuth = response.body();
                if(response.code() == 200) {

                    if (policeAuth.isNumberVerified())
                        startLogin();
                    else if (!policeAuth.isNumberVerified()) {
                        Toast.makeText(AuthActivity.this, R.string.not_a_police_admin, Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<PoliceAuth> call, Throwable t) {


            }
        });

    }

    private void startLogin() {
        phoneNumber = "+91" + phoneNumber;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }
}
