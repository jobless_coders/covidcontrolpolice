package com.joblesscoders.covidcontrolpolice.nonadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.admin.PoliceStationRegistrationActivity;
import com.joblesscoders.covidcontrolpolice.auth.MainActivity;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;
import com.joblesscoders.covidcontrolpolice.pojo.PoliceStation;
import com.joblesscoders.covidcontrolpolice.pojo.Reply;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NonAdminRegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    private List<PoliceStation> policeStationList = new ArrayList<>();
    private EditText name,pin;
    private View register;
    private Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_admin_registration);

        spinner = (Spinner) findViewById(R.id.police_station_name);
        name = findViewById(R.id.name);
        pin = findViewById(R.id.pin);
        register = findViewById(R.id.register);
        register.setOnClickListener(this);


        final List<String> categories = new ArrayList<String>();
        categories.add("Select a police station");


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_dropdown_item_1line,categories);

        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        RestApiHandler.getAPIService().getAllPoliceStation().enqueue(new Callback<Reply>() {
            @Override
            public void onResponse(Call<Reply> call, Response<Reply> response) {
                for(PoliceStation policeStation:response.body().getData())
                {
                    //if(policeStation.getHeadName()==null||policeStation.getHeadName().length()==0) {
                        policeStationList.add(policeStation) ;
                        categories.add(policeStation.getPoliceStationName());
                   // }
                }
              //  Toast.makeText(NonAdminRegistrationActivity.this, response.body().getData().size()+"", Toast.LENGTH_SHORT).show();
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<Reply> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        if(name.getText().toString().trim().length()==0)
        {
            Toast.makeText(this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(pin.getText().toString().trim().length()==0)
        {
            Toast.makeText(this, "Pin cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(spinner.getSelectedItemPosition()==0)//.getText().toString().trim().length()==0)
        {
            Toast.makeText(this, "Select a police station first", Toast.LENGTH_SHORT).show();
            return;
        }
        PoliceStation policeStation = policeStationList.get(spinner.getSelectedItemPosition()-1);
        if(policeStation.getPin()!=Integer.parseInt(pin.getText().toString().trim()))
        {
            Toast.makeText(this, "Pin doesn't match", Toast.LENGTH_SHORT).show();
            return;
        }

        PolicePersonnel policePersonnel = new PolicePersonnel(name.getText().toString().trim(),policeStation.getPoliceStationName(), FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3),policeStation.getId(),false,Integer.parseInt(pin.getText().toString().trim()));
        RestApiHandler.getAPIService().createPolicePersonnel(policePersonnel).enqueue(new Callback<Reply>() {
            @Override
            public void onResponse(Call<Reply> call, Response<Reply> response) {
                Toast.makeText(NonAdminRegistrationActivity.this, response.body().getMessage()+"", Toast.LENGTH_SHORT).show();
                if(response.code() == 200&&response.body().getMessage().equals("success"))
                {
                    Intent intent = new Intent(NonAdminRegistrationActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Toast.makeText(NonAdminRegistrationActivity.this, "Police registered successfully!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Reply> call, Throwable t) {

            }
        });
    }
}
