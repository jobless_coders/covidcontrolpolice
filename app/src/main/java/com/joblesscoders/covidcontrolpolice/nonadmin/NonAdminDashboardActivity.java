package com.joblesscoders.covidcontrolpolice.nonadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.admin.AdminDashboardActivity;
import com.joblesscoders.covidcontrolpolice.auth.MainActivity;
import com.joblesscoders.covidcontrolpolice.pojo.Reply;
import com.joblesscoders.covidcontrolpolice.profile.ProfileActivity;
import com.joblesscoders.covidcontrolpolice.report.ReportActivity;
import com.joblesscoders.covidcontrolpolice.showpolice.ShowPolicePersonnelActivity;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NonAdminDashboardActivity extends AppCompatActivity implements View.OnClickListener {
    private View all_report,active_report,logout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_non_admin);

        Spannable text = new SpannableString("Dashboard");
        text.setSpan(new ForegroundColorSpan(Color.BLACK), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(text);
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( getResources().getColor(R.color.white )) );
        active_report = findViewById(R.id.active_report);
        all_report = findViewById(R.id.all_report);
      //  logout = findViewById(R.id.logout);
      //  logout.setOnClickListener(this);
        active_report.setOnClickListener(this);
        all_report.setOnClickListener(this);
        initFCM();
        //Toast.makeText(this, "nonadmin", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.all_report:
                Intent intent = new Intent(NonAdminDashboardActivity.this, ReportActivity.class);
                intent.putExtra("type", "all");
                startActivity(intent);
                break;
            case R.id.active_report:
                intent = new Intent(NonAdminDashboardActivity.this, ReportActivity.class);
                intent.putExtra("type", "active");
                startActivity(intent);
                break;

        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_logout:
                new AlertDialog.Builder(NonAdminDashboardActivity.this)
                        .setTitle("Logout")
                        .setMessage("Are you sure you want to logout?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                logoutPolice();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .show();
                return true;
            case R.id.action_profile:
                startActivity(new Intent(this, ProfileActivity.class));

                return  true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logoutPolice() {
        SharedPreferences settings = getApplicationContext().getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        settings.edit().clear().commit();
        FirebaseAuth.getInstance().signOut();
        Toast.makeText(this, "Logged out successfully", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }
    private void initFCM() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            //   Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        RestApiHandler.getAPIService().storeToken(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3),token).enqueue(new Callback<Reply>() {
                            @Override
                            public void onResponse(Call<Reply> call, Response<Reply> response) {
                                if(response.code() == 200&&response.body().getMessage().equals("success"))
                                {
                                    Toast.makeText(NonAdminDashboardActivity.this, "Token updated", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Reply> call, Throwable t) {

                            }
                        });

                        // Log and toast
                        // String msg = getString(R.string.msg_token_fmt, token);
                        // Log.d(TAG, msg);
                        //  Toast.makeText(AdminDashboardActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

    }
}
