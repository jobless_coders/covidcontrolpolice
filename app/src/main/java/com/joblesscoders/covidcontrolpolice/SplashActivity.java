package com.joblesscoders.covidcontrolpolice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.joblesscoders.covidcontrolpolice.admin.AdminDashboardActivity;
import com.joblesscoders.covidcontrolpolice.auth.MainActivity;
import com.joblesscoders.covidcontrolpolice.nonadmin.NonAdminDashboardActivity;
import com.joblesscoders.covidcontrolpolice.utils.SharedPrefHandler;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(SharedPrefHandler.isType(getApplicationContext())==null)
                {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
                else if(SharedPrefHandler.isType(getApplicationContext()).equals("admin"))
                {
                    startActivity(new Intent(SplashActivity.this, AdminDashboardActivity.class));
                    finish();
                }
                else{
                    startActivity(new Intent(SplashActivity.this, NonAdminDashboardActivity.class));
                    finish();
                }
            }
        },2000);
    }
}
