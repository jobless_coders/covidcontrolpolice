package com.joblesscoders.covidcontrolpolice.report;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;
import com.joblesscoders.covidcontrolpolice.pojo.Reply;
import com.joblesscoders.covidcontrolpolice.pojo.Report;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;
import com.joblesscoders.covidcontrolpolice.utils.SharedPrefHandler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportDisplayActivity extends AppCompatActivity {
    private TextView name,type,report_time,phone,police_station_name,address,description,attendedby,attended_time;
    private MaterialButton call,map,mark_attended;
    private ImageView imageView;
    private View attendedll,detailsll;
    private Report report;
    private PolicePersonnel policePersonnel;
    private  SimpleDateFormat simpleDateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_display);
        policePersonnel = SharedPrefHandler.getPoliceData(getApplicationContext());
         simpleDateFormat = new SimpleDateFormat("hh:mm:ss a, dd-MM-yyyy");
        report = getIntent().getExtras().getParcelable("report");
        name = findViewById(R.id.username);
        type = findViewById(R.id.type);
        report_time = findViewById(R.id.time);
        phone = findViewById(R.id.phone);
        police_station_name = findViewById(R.id.police_station_name);
        address = findViewById(R.id.address);
        description = findViewById(R.id.description);
        attendedby = findViewById(R.id.attended_by);
        attended_time = findViewById(R.id.attended_time);
        call = findViewById(R.id.call);
        map = findViewById(R.id.map);
        mark_attended = findViewById(R.id.attended);
        imageView = findViewById(R.id.image);
        attendedll = findViewById(R.id.attendedll);
        detailsll = findViewById(R.id.detailsll);

        Glide.with(getApplicationContext())
                .load(report.getImageUrl())
                .centerCrop()
                .into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReportDisplayActivity.this, ImageDisplayActivity.class);
                intent.putExtra("imageUrl",report.getImageUrl());
                startActivity(intent);
            }
        });

        name.setText(report.getUsername());
        type.setText("Type of report : "+report.getReportType());
        report_time.setText("Time of report : "+simpleDateFormat.format(report.getTimestamp()));
        phone.setText("Phone no : "+report.getPhone());
        police_station_name.setText("P.S : "+report.getPoliceStationName());
        address.setText(getCompleteAddressString(report.getLatitude(),report.getLongitude()).trim());
        if(report.getReportDetails().trim().length()==0) {
            description.setVisibility(View.GONE);
            detailsll.setVisibility(View.GONE);
        }
        else
        description.setText(report.getReportDetails());
        if(report.isAttended())
        {
            attended_time.setText("Attended at time : "+simpleDateFormat.format(report.getAttendedTime()));
            attendedby.setText("Attended by : "+report.getAttendedByName());
            mark_attended.setVisibility(View.GONE);
        }
        else {
            attendedll.setVisibility(View.GONE);
        }

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+report.getPhone()));
                intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);
            }
        });
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = "https://www.google.com/maps/search/?api=1&query="+report.getLatitude()+","+report.getLongitude();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,Uri.parse(url));
                intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);

            }
        });
        mark_attended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Report postReport = new Report(policePersonnel.getName(),report.get_id(),policePersonnel.get_id(),new Date().getTime(),true);

                RestApiHandler.getAPIService().updateReport(postReport).enqueue(new Callback<Reply>() {
                    @Override
                    public void onResponse(Call<Reply> call, Response<Reply> response) {
                       // Toast.makeText(ReportDisplayActivity.this, response.body().getMessage()+"", Toast.LENGTH_SHORT).show();
                        if(response.code() == 200&&response.body().getMessage().equals("success"))
                        {
                            Toast.makeText(ReportDisplayActivity.this, "Reported marked attended", Toast.LENGTH_SHORT).show();
                            updateUI(postReport);
                        }
                    }

                    @Override
                    public void onFailure(Call<Reply> call, Throwable t) {

                    }
                });
            }
        });




    }

    private void updateUI(Report postReport) {
        mark_attended.setIcon(null);
        mark_attended.setText("Attended");
        mark_attended.setBackgroundColor(getResources().getColor(R.color.white));
        mark_attended.setTextColor(getResources().getColor(R.color.colorPrimary));
        mark_attended.setStrokeColor(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
        mark_attended.setStrokeWidth(2);
        mark_attended.setEnabled(false);
        attendedll.setVisibility(View.VISIBLE);
        attended_time.setText("Attended at time : "+simpleDateFormat.format(postReport.getAttendedTime()));
        attendedby.setText("Attended by : "+postReport.getAttendedByName());

    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }
}
