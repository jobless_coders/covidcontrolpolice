package com.joblesscoders.covidcontrolpolice.report;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.joblesscoders.covidcontrolpolice.R;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

public class ImageDisplayActivity extends AppCompatActivity {
private ImageView displayImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_display);
        String url = getIntent().getExtras().getString("imageUrl");
        if (url == null) {
            Toast.makeText(this, "Invalid image link provided!", Toast.LENGTH_SHORT).show();
            finish();
        }
        displayImage = findViewById(R.id.displayImage);

        Glide.with(getApplicationContext())
                .load(url)
                .into(displayImage);
    }
}
