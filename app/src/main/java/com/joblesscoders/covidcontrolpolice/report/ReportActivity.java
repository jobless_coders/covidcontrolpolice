package com.joblesscoders.covidcontrolpolice.report;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;
import com.joblesscoders.covidcontrolpolice.pojo.Report;
import com.joblesscoders.covidcontrolpolice.pojo.ReportReply;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;
import com.joblesscoders.covidcontrolpolice.utils.SharedPrefHandler;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private String type;
    private List<Report> reportList = new ArrayList<>();
    private PolicePersonnel policePersonnel;
    private ReportAdapter reportAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        recyclerView = findViewById(R.id.recyclerview);
        toolbar = findViewById(R.id.title);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recyclerview);
        policePersonnel = SharedPrefHandler.getPoliceData(getApplicationContext());
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        reportAdapter = new ReportAdapter(reportList,getApplicationContext());
        recyclerView.setAdapter(reportAdapter);
        type = getIntent().getExtras().getString("type");

        //initData();



    }

    private void initData() {


        if(type.equals("all")&&reportList.size()==0) {
            getSupportActionBar().setTitle("Past Reports");
            RestApiHandler.getAPIService().getReports(policePersonnel.getPoliceStationId(),true).enqueue(new Callback<ReportReply>() {
                @Override
                public void onResponse(Call<ReportReply> call, Response<ReportReply> response) {
                    if(response.code() == 200)
                    {
                        reportList.addAll(response.body().getData());
                        reportAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<ReportReply> call, Throwable t) {

                }
            });
        }
        else if(type.equals("active")) {
            reportList.clear();
            getSupportActionBar().setTitle("Active Reports");
            RestApiHandler.getAPIService().getReports(policePersonnel.getPoliceStationId(),false).enqueue(new Callback<ReportReply>() {
                @Override
                public void onResponse(Call<ReportReply> call, Response<ReportReply> response) {
                    // Toast.makeText(ReportActivity.this, response.body().getData().size()+"", Toast.LENGTH_SHORT).show();
                    if(response.code() == 200)
                    {
                        reportList.addAll(response.body().getData());
                        reportAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<ReportReply> call, Throwable t) {

                }
            });
        }
    }

    @Override
    protected void onResume() {
        initData();
        super.onResume();
    }


}
