package com.joblesscoders.covidcontrolpolice.report;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;
import com.joblesscoders.covidcontrolpolice.pojo.Reply;
import com.joblesscoders.covidcontrolpolice.pojo.Report;
import com.joblesscoders.covidcontrolpolice.showpolice.ShowPoliceAdapter;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportAdapter extends RecyclerView.Adapter {
    private List<Report> reportList;
    private Context context;

    public ReportAdapter(List<Report> reportList, Context context) {
        this.reportList = reportList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.layout_report, parent, false);
        ReportHolder viewHolder = new ReportHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final Report report = reportList.get(position);
        final ReportHolder reportHolder = (ReportHolder) holder;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm a, dd-MM-yyyy");
        reportHolder.name.setText(report.getUsername());
        reportHolder.type.setText("Type : " + report.getReportType());
        reportHolder.address.setText(getCompleteAddressString(report.getLatitude(),report.getLongitude()).trim());
        reportHolder.time.setText( simpleDateFormat.format(report.getTimestamp()));

       /* if (policePersonnel.isApproved()) {
            updateUI(policeHolder);
          /*  policeHolder.approve.setIcon(null);
            policeHolder.approve.setText("Approved");
            policeHolder.approve.setBackgroundColor(context.getResources().getColor(R.color.white));
        }*/
        Glide
                .with(context)
                .load(report.getImageUrl())
                .centerCrop()

                .into(reportHolder.imageView);
        reportHolder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+report.getPhone()));
                intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                context.startActivity(intent);
            }
        });
        reportHolder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String url = "https://www.google.com/maps/search/?api=1&query="+report.getLatitude()+","+report.getLongitude();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,Uri.parse(url));
                intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                context.startActivity(intent);

            }
        });
        reportHolder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,ReportDisplayActivity.class);
                intent.putExtra("report",report);
                intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                context.startActivity(intent);
            }
        });






    }
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }
    public class ReportHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name,time,type,address;
        public MaterialButton call,map;
        private ImageView imageView;
        private View rl;


        public ReportHolder(View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            name = itemView.findViewById(R.id.name);
            type = itemView.findViewById(R.id.type);
            address = itemView.findViewById(R.id.address);
            call = itemView.findViewById(R.id.call);
            map = itemView.findViewById(R.id.map);
            imageView = itemView.findViewById(R.id.image);
            rl = itemView.findViewById(R.id.rl);


            /*call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bookItemClickListener.onItemClick(v, getAdapterPosition());

                }
            });
            approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bookItemClickListener.onItemClick(v, getAdapterPosition());

                }
            });*/
        }

        @Override
        public void onClick(View v) {


        }
    }
}
