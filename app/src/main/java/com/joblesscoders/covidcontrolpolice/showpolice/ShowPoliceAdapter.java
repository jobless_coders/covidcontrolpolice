package com.joblesscoders.covidcontrolpolice.showpolice;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;
import com.joblesscoders.covidcontrolpolice.pojo.Reply;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowPoliceAdapter extends RecyclerView.Adapter {
    private List<PolicePersonnel> policePersonnelList;
    private Context context;

    public ShowPoliceAdapter(List<PolicePersonnel> policePersonnelList, Context context) {
        setHasStableIds(true);
        //Toast.makeText(context, "gg", Toast.LENGTH_SHORT).show();
        this.context = context;
        this.policePersonnelList = policePersonnelList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.layout_show_police, parent, false);
        PoliceHolder viewHolder = new PoliceHolder(listItem);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        //Toast.makeText(context, "ff", Toast.LENGTH_SHORT).show();
        final PolicePersonnel policePersonnel = policePersonnelList.get(position);
        final PoliceHolder policeHolder = (PoliceHolder) holder;
        policeHolder.name.setText(policePersonnel.getName());
        policeHolder.phone.setText("Phone : " + policePersonnel.getPhone());
        policeHolder.policestationname.setText("P.S : " + policePersonnel.getPoliceStationName());
        if (policePersonnel.isApproved()) {
            updateUI(policeHolder);
          /*  policeHolder.approve.setIcon(null);
            policeHolder.approve.setText("Approved");
            policeHolder.approve.setBackgroundColor(context.getResources().getColor(R.color.white));*/
        }
        policeHolder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+policePersonnel.getPhone()));
                intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                context.startActivity(intent);
            }
        });
        policeHolder.approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(policePersonnel.isApproved())
                    return;

                RestApiHandler.getAPIService().updatePolicePersonnel(true,policePersonnel.get_id()).enqueue(new Callback<Reply>() {
                    @Override
                    public void onResponse(Call<Reply> call, Response<Reply> response) {
                        if(response.code() == 200)
                        {
                            updateUI(policeHolder);
                        }
                    }

                    @Override
                    public void onFailure(Call<Reply> call, Throwable t) {

                    }
                });


            }
        });

    }

    private void updateUI(PoliceHolder policeHolder) {
        policeHolder.approve.setIcon(null);
        policeHolder.approve.setText("Approved");
        policeHolder.approve.setBackgroundColor(context.getResources().getColor(R.color.white));
        policeHolder.approve.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        policeHolder.approve.setStrokeColor(ColorStateList.valueOf(context.getResources().getColor(R.color.colorPrimary)));
        policeHolder.approve.setStrokeWidth(1);
        policeHolder.approve.setEnabled(false);
    }

    @Override
    public int getItemCount() {
        return policePersonnelList.size();
    }
    public class PoliceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name,phone,policestationname;
        public MaterialButton call,approve;


        public PoliceHolder(View itemView) {
            super(itemView);
            policestationname = itemView.findViewById(R.id.police_station_name);
            name = itemView.findViewById(R.id.name);
            phone = itemView.findViewById(R.id.phone);
            call = itemView.findViewById(R.id.call);
            approve = itemView.findViewById(R.id.approve);


            /*call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bookItemClickListener.onItemClick(v, getAdapterPosition());

                }
            });
            approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bookItemClickListener.onItemClick(v, getAdapterPosition());

                }
            });*/
        }

        @Override
        public void onClick(View v) {


        }
    }

}
