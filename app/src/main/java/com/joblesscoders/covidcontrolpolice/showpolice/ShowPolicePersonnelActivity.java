package com.joblesscoders.covidcontrolpolice.showpolice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnelReply;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;
import com.joblesscoders.covidcontrolpolice.utils.SharedPrefHandler;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowPolicePersonnelActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private String type;
    private List<PolicePersonnel> policePersonnelList = new ArrayList<>();
    private PolicePersonnel policePersonnel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_police_personnel);
        toolbar = findViewById(R.id.title);
        setSupportActionBar(toolbar);
        type = getIntent().getExtras().getString("type");
        if(type.equals("all"))
        getSupportActionBar().setTitle("All Police Personnel");
        else
            getSupportActionBar().setTitle("Approve Police Personnel");
//        toolbar = findViewById(R.id.title);
        //toolbar.setTitle("Show all police");

        recyclerView = findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        final ShowPoliceAdapter showPoliceAdapter = new ShowPoliceAdapter(policePersonnelList,getApplicationContext());
        recyclerView.setAdapter(showPoliceAdapter);
         policePersonnel = SharedPrefHandler.getPoliceData(getApplicationContext());
        if(type.equals("all")) {
            RestApiHandler.getAPIService().getAllPolicePersonnel(policePersonnel.getPoliceStationId(),true).enqueue(new Callback<PolicePersonnelReply>() {
                @Override
                public void onResponse(Call<PolicePersonnelReply> call, Response<PolicePersonnelReply> response) {
                    if (response.code() == 200) {
                        policePersonnelList.addAll(response.body().getData());
                     //   Toast.makeText(ShowPolicePersonnelActivity.this, policePersonnelList.size() + "", Toast.LENGTH_SHORT).show();
                        showPoliceAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<PolicePersonnelReply> call, Throwable t) {

                }
            });
        }
        else {
            RestApiHandler.getAPIService().getAllPolicePersonnel(policePersonnel.getPoliceStationId(),false).enqueue(new Callback<PolicePersonnelReply>() {
                @Override
                public void onResponse(Call<PolicePersonnelReply> call, Response<PolicePersonnelReply> response) {
                    if(response.code() == 200)
                    {
                        policePersonnelList.addAll(response.body().getData());
                      //  Toast.makeText(ShowPolicePersonnelActivity.this, policePersonnelList.size() + "", Toast.LENGTH_SHORT).show();
                        showPoliceAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onFailure(Call<PolicePersonnelReply> call, Throwable t) {

                }
            });
        }


    }
}
