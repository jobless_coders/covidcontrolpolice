package com.joblesscoders.covidcontrolpolice.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.auth.MainActivity;
import com.joblesscoders.covidcontrolpolice.pojo.PoliceStation;
import com.joblesscoders.covidcontrolpolice.pojo.Reply;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PoliceStationRegistrationActivity extends AppCompatActivity implements View.OnClickListener {
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest locationRequest;
    private LatLng currentLocation;
    private static final int LOCATION = 1001;
    private static final int REQUEST_CHECK_SETTINGS = 99 ;

    private EditText policeStationName,adminName,address;
    private View register;
    private String phone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
         phone = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
        policeStationName = findViewById(R.id.police_station_name);
        adminName = findViewById(R.id.admin_name);
        address = findViewById(R.id.address);
        register = findViewById(R.id.register);
        register.setOnClickListener(this);
        //Toast.makeText(this, phone+"", Toast.LENGTH_SHORT).show();

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {

                    currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    //Toast.makeText(PoliceStationRegistrationActivity.this, currentLocation.latitude+"", Toast.LENGTH_SHORT).show();
                    stopLocationUpdates();

                }

            };
        };

    }
    @Override
    protected void onResume() {
        super.onResume();

        checkGPS();



    }

    private void checkGPS(){

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        // LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                if(isLocationEnabled())
                {

                    startLocationUpdates();

                }
                else {
                    checkPermissionLocation();

                }

            }
        });

        task.addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(PoliceStationRegistrationActivity.this,
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });

    }
    private void checkPermissionLocation() {

        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;

        if (permissionAccessCoarseLocationApproved) {

            startLocationUpdates();
        } else {
            // App doesn't have access to the device's location at all. Make full request
            // for permission.
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION

                    },
                    LOCATION);
        }
    }

    private boolean isLocationEnabled() {

        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;

        if(permissionAccessCoarseLocationApproved)
            return true;
        else return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case LOCATION:

                if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION))
                    startLocationUpdates();
//                else
//                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                break;


        }

    }


    private void startLocationUpdates() {

        fusedLocationClient.requestLocationUpdates(locationRequest,
                locationCallback,
                Looper.getMainLooper());

    }
    private void stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }




    @Override
    public void onClick(View view) {
        if(policeStationName.getText().toString().trim().length()==0) {
            Toast.makeText(this, "Police station name cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(adminName.getText().toString().trim().length()==0) {
            Toast.makeText(this, "Admin name cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(address.getText().toString().trim().length()==0) {
            Toast.makeText(this, "Address cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(currentLocation==null)
        {
            Toast.makeText(this, "Provide location", Toast.LENGTH_SHORT).show();
            startLocationUpdates();
            return;
        }
        PoliceStation policeStation = new PoliceStation(adminName.getText().toString().trim(),policeStationName.getText().toString().trim(), FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber().substring(3),address.getText().toString().trim(),currentLocation.latitude,currentLocation.longitude);
        RestApiHandler.getAPIService().createPoliceStation(policeStation).enqueue(new Callback<Reply>() {
            @Override
            public void onResponse(Call<Reply> call, Response<Reply> response) {
                if(response.code() == 200)
                {
                    Toast.makeText(PoliceStationRegistrationActivity.this, "Police station registered", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PoliceStationRegistrationActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }
            }

            @Override
            public void onFailure(Call<Reply> call, Throwable t) {

            }
        });

    }
}
