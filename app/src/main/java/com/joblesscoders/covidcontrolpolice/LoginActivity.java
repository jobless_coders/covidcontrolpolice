package com.joblesscoders.covidcontrolpolice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.joblesscoders.covidcontrolpolice.auth.OtpEditText;
import com.joblesscoders.covidcontrolpolice.pojo.PoliceAuth;
import com.joblesscoders.covidcontrolpolice.utils.RestApiHandler;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
@Deprecated
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private FirebaseAuth mAuth;
    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String phoneNumber;
    private View send_otp,verify_otp,otpll,phonell,resend_otp;
    private EditText phone;
    private OtpEditText otpEditText;
    private PoliceAuth policeAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        otpEditText = findViewById(R.id.et_otp);
        send_otp = findViewById(R.id.send);
        send_otp.setOnClickListener(this);
        verify_otp = findViewById(R.id.verifyotp);
        resend_otp = findViewById(R.id.resendotp);
        otpll = findViewById(R.id.otpll);
       // phonell = findViewById(R.id.phonell);
        phonell = findViewById(R.id.phonell);
        verify_otp.setOnClickListener(this);
        resend_otp.setOnClickListener(this);
        phone = findViewById(R.id.phone);

       mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);
               // Toast.makeText(LoginActivity.this, "hhh", Toast.LENGTH_SHORT).show();

                signInWithPhoneAuthCredential(credential);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);
                Toast.makeText(LoginActivity.this, e.getMessage()+"", Toast.LENGTH_SHORT).show();



                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;
                phonell.setVisibility(View.GONE);
                otpll.setVisibility(View.VISIBLE);


                // ...
            }
        };



    }
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
      //  FirebaseU mAuth;
         mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(LoginActivity.this, R.string.otp_verified, Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                              if (policeAuth.isNumberVerified() && !policeAuth.isNumberRegistered()) {
                                //start registration
                            } else if (policeAuth.isNumberVerified() && policeAuth.isNumberRegistered()) {
                                //start login
                            }
                            // ...
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(LoginActivity.this, "Verification code was invalid", Toast.LENGTH_SHORT).show();

                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
            phoneNumber = phone.getText().toString();
            if (phoneNumber.length() < 10) {
                Toast.makeText(this, "Not a valid phone number!", Toast.LENGTH_SHORT).show();
                return;
            }

            RestApiHandler.getAPIService().loginPoliceAdmin(phoneNumber).enqueue(new Callback<PoliceAuth>() {
                @Override
                public void onResponse(Call<PoliceAuth> call, Response<PoliceAuth> response) {
                     policeAuth = response.body();
                    if(response.code() == 200) {

                        if (policeAuth.isNumberVerified())
                            startLogin();
                        else if (!policeAuth.isNumberVerified()) {
                            Toast.makeText(LoginActivity.this, R.string.not_a_police_admin, Toast.LENGTH_SHORT).show();
                        }
                    }

                }

                @Override
                public void onFailure(Call<PoliceAuth> call, Throwable t) {


                }
            });

                break;
            case R.id.resendotp:
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        phoneNumber,        // Phone number to verify
                        60,                 // Timeout duration
                        TimeUnit.SECONDS,   // Unit of timeout
                        this,               // Activity (for callback binding)
                        mCallbacks,mResendToken);        // OnVerificationStateChangedCallbacks

                break;
            case R.id.verifyotp:
                String code = otpEditText.getText().toString();
                if(code.length()<6)
                {
                    Toast.makeText(this, "OTP invalid", Toast.LENGTH_SHORT).show();
                    return;
                }
                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
                signInWithPhoneAuthCredential(credential);
                break;
        }
    }

    private void startLogin() {
        phoneNumber = "+91" + phoneNumber;
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }
}
