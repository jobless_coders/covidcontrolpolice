package com.joblesscoders.covidcontrolpolice.profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.joblesscoders.covidcontrolpolice.R;
import com.joblesscoders.covidcontrolpolice.pojo.PolicePersonnel;
import com.joblesscoders.covidcontrolpolice.utils.SharedPrefHandler;

public class ProfileActivity extends AppCompatActivity {
    private TextView pin,name,phone,type,police_station_name;
    private MaterialButton sharepin;
    private PolicePersonnel policePersonnel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        policePersonnel = SharedPrefHandler.getPoliceData(getApplicationContext());
        pin = findViewById(R.id.pin);
        name = findViewById(R.id.name);
        type = findViewById(R.id.type);
        police_station_name = findViewById(R.id.police_station_name);
        phone = findViewById(R.id.phone);
        sharepin = findViewById(R.id.sharepin);
        sharepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            shareText(getSharableMessage(policePersonnel));
            }
        });
        police_station_name.setText("P.S : "+policePersonnel.getPoliceStationName());

        name.setText("Name : "+policePersonnel.getName());
        type.setText("Type : "+policePersonnel.getType());
        phone.setText("Phone number : " +FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
        if(!policePersonnel.getType().equals("admin"))
        {
            pin.setVisibility(View.GONE);
            sharepin.setVisibility(View.GONE);
        }
        else if(policePersonnel.getPin()!=0)
        {
           pin.setText("Pin : "+policePersonnel.getPin()+"");
        }


    }
    public String getSharableMessage(PolicePersonnel personnel) {
        String message = "";
        String url = "https://drive.google.com/open?id=17oRHMOjJwT4lz9chg5uqYzoLTxRFmpDo";

        message = "Hello everybody,\nPlease download the app from the given link.\n\n";
        message += url + "\n\n";
        message += "1. Open the app, and select *Nonadmin*\n";
        message += "2. Enter your phone number\n";
        message += "3. Select the branch as *" + personnel.getPoliceStationName()+"*\n";
        message += "4. Enter the pin *" + personnel.getPin() + "*";

        return message;


    }
    public void shareText(String body) {
        String subject = "Use this PIN during registration";
        Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
        txtIntent .setType("text/plain");
        txtIntent .putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        txtIntent .putExtra(android.content.Intent.EXTRA_TEXT, body);
        startActivity(Intent.createChooser(txtIntent ,"Share Pin"));
    }
}
